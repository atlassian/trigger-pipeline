import enum
import os
import time
import urllib.parse
from http import HTTPStatus

import requests
import yaml
from requests.adapters import HTTPAdapter, Retry

from bitbucket_pipes_toolkit import Pipe, get_logger


BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'
BITBUCKET_INTERNAL_BASE_URL = 'https://api.bitbucket.org/internal'
REQUEST_CONNECT_TIMEOUT = 10  # in seconds  https://requests.readthedocs.io/en/latest/user/advanced/#timeouts
REQUEST_READ_TIMEOUT = 10  # in seconds  https://requests.readthedocs.io/en/latest/user/advanced/#timeouts
# SLEEP TIME in seconds before the next check build statuses on Bitbucket Cloud
BITBUCKET_API_POLLING_INTERVAL = 5  # in seconds
MAX_REQUEST_RETRIES = 5  # integer  https://urllib3.readthedocs.io/en/stable/reference/urllib3.util.html#urllib3.util.Retry
REQUEST_BACKOFF_FACTOR = 1  # number
RETRY_ALLOWED_METHODS = frozenset([*Retry.DEFAULT_ALLOWED_METHODS, 'POST'])

logger = get_logger()

# See https://developer.atlassian.com/cloud/bitbucket/rest/api-group-pipelines/#api-repositories-workspace-repo-slug-pipelines-post
schema = {'BITBUCKET_USERNAME': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
          'BITBUCKET_APP_PASSWORD': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
          'BITBUCKET_ACCESS_TOKEN': {'required': False, 'type': 'string',
                                     'excludes': ['BITBUCKET_USERNAME', 'BITBUCKET_APP_PASSWORD']},
          'ACCOUNT': {'nullable': True, 'required': False, 'type': 'string',
                      'default': os.getenv('BITBUCKET_REPO_OWNER')},
          'REF_NAME': {'type': 'string', 'default': 'master', 'dependencies': ['REF_TYPE']},
          'REF_TYPE': {
              'allowed': ['branch', 'tag'],
              'default': 'branch',
              'type': 'string',
              'dependencies': ['REF_NAME']},
          'COMMIT': {
              'type': 'string',
              'required': False},
          'CUSTOM_PIPELINE_NAME': {
              'type': 'string',
              'required': False,
              'nullable': True,
              'default': None},
          'PIPELINE_VARIABLES': {
              'required': False,
              'nullable': True,
              'default': None},
          'REPOSITORY': {'required': True, 'type': 'string'},
          'WAIT': {'default': False, 'type': 'boolean'},
          'WAIT_MAX_TIMEOUT': {'default': 3600, 'type': 'integer'},
          'REQUEST_READ_TIMEOUT': {'default': REQUEST_READ_TIMEOUT, 'type': 'integer'},
          'MAX_RETRIES_ON_FAILURE': {'required': False, 'type': 'integer',
                                     'min': 1, 'max': 10,
                                     'dependencies': ['WAIT', 'BITBUCKET_USERNAME', 'BITBUCKET_APP_PASSWORD']},
          'DEBUG': {'default': False, 'type': 'boolean'}}


@enum.unique
class BuildStatus(enum.Enum):
    parsing = 'PARSING'
    completed = 'COMPLETED'
    succeeded = 'SUCCESSFUL'
    failed = 'FAILED'
    pending = 'PENDING'
    in_progress = 'IN_PROGRESS'
    paused = 'PAUSED'
    halted = 'HALTED'


class TriggerBuild(Pipe):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.account = self.get_variable('ACCOUNT')
        self.repo = self.get_variable('REPOSITORY').lower()
        self.ref_type = self.get_variable('REF_TYPE')
        self.ref_name = self.get_variable('REF_NAME')
        self.commit = self.get_variable('COMMIT')
        self.custom_pipeline = self.get_variable('CUSTOM_PIPELINE_NAME')
        self.pipeline_variables = self.get_variable('PIPELINE_VARIABLES')
        self.wait = self.get_variable('WAIT')
        self.wait_max_time = self.get_variable('WAIT_MAX_TIMEOUT')
        self.request_read_timeout = self.get_variable('REQUEST_READ_TIMEOUT')
        self.max_retries_on_failure = self.get_variable('MAX_RETRIES_ON_FAILURE')

        self.headers = {'Content-Type': 'application/json'}

    def _make_session_request(self, method, url, error_base_message, **kwargs):
        try:
            with requests.Session() as s:
                retries = Retry(total=MAX_REQUEST_RETRIES, allowed_methods=RETRY_ALLOWED_METHODS, backoff_factor=REQUEST_BACKOFF_FACTOR)
                s.mount('https://', HTTPAdapter(max_retries=retries))
                return s.request(method, url, timeout=(REQUEST_CONNECT_TIMEOUT, self.request_read_timeout), **kwargs)
        except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout) as e:
            message = f"{error_base_message} Error: {e}"
            self.fail(message)
        except requests.exceptions.RequestException as e:
            message = f"{error_base_message} Error: {e}"
            self.fail(message)

    def _check_build(self, build_number, auth):
        build_url = f'{BITBUCKET_API_BASE_URL}/repositories/{self.account}/{self.repo}/pipelines/{build_number}'
        error_base_message = f"Failed to get the build state for build number {build_number} for {self.repo}."
        response = self._make_session_request("GET", build_url, error_base_message, auth=auth)

        if not response.status_code == HTTPStatus.OK:
            message = (f"{error_base_message}"
                       f" "
                       f"Status Code: {response.status_code}. Error: {response.text}")
            self.log_error(f"{message}. Retrying...")
            return None, message

        build_data = response.json()
        build_state = build_data['state']['name']

        logger.debug(f"Build state: {build_data['state']}")

        if build_state == BuildStatus.parsing.value:
            logger.info(
                f"Build number {build_number} for {self.repo} is {build_state}, "
                f"waiting for pipeline to be parsed...")

            return None, None

        elif build_state == BuildStatus.completed.value:
            result = build_data['state']['result']['name']

            if result == BuildStatus.succeeded.value:
                return True, f"Build number {build_number} for {self.repo}@{self.ref_name} finished successfully."
            elif result == BuildStatus.failed.value:
                return False, f"Build number {build_number} for {self.repo}@{self.ref_name} failed."
            else:
                return False, (f"Build number {build_number} for {self.repo}@{self.ref_name} "
                               f"finished with result {result}.")

        elif build_state in (BuildStatus.pending.value, BuildStatus.in_progress.value):
            logger.info(
                f"Build number {build_number} for {self.repo} is {build_state}, "
                f"waiting for pipeline to finish...")

            if build_data['state']['stage']['name'] == BuildStatus.paused.value:
                return True, f"Build number {build_number} for {self.repo}@{self.ref_name} is paused."
            elif build_data['state']['stage']['name'] == BuildStatus.halted.value:
                return False, f"Build number {build_number} for {self.repo}@{self.ref_name} is halted."
            else:
                return None, None

        else:
            return False, f"Unexpected pipeline build state: {build_state}"

    def _get_tag_info(self, tag_name, account, repo, auth):
        logger.info(f'Fetching the tag info for tag:{tag_name}')
        tag_url = f'{BITBUCKET_API_BASE_URL}/repositories/{account}/{repo}/refs/tags/{tag_name}'
        error_base_message = f"Failed to get the tag info for {tag_name} from Bitbucket."
        response = self._make_session_request("GET", tag_url, error_base_message, auth=auth)

        if not response.status_code == HTTPStatus.OK:
            self.fail(
                f'Failed to get the tag info for {tag_name} from Bitbucket. Response code: {response.status_code}. Error: {response.text}')
        return response.json()

    def _get_tag_target(self, tag_name, account, repo, auth):
        return self._get_tag_info(tag_name, account, repo, auth)['target']['hash']

    def _retry_completed_failed_pipeline(self, build_uuid, auth):
        error_base_message = f'Failed to retry pipeline: https://bitbucket.org/{self.account}/{self.repo}/addon/pipelines/home#!/results/{build_uuid}'
        retry_url = f'{BITBUCKET_INTERNAL_BASE_URL}/repositories/{self.account}/{self.repo}/pipelines/{urllib.parse.quote_plus(build_uuid)}/runs'
        response = self._make_session_request("POST", retry_url, error_base_message, auth=auth)

        if not response.status_code == HTTPStatus.CREATED:
            self.fail(error_base_message)

    def run(self):
        super().run()

        auth = self.resolve_auth()

        target = {
            'type': 'pipeline_ref_target',
            'ref_type': self.ref_type,
            'ref_name': self.ref_name
        }
        data = {'target': target}

        if self.commit:
            target['commit'] = {
                'type': 'commit',
                'hash': self.commit
            }
        # Bitbucket Pipelines API has a bug where a tag alone, without explicit commit throws HTTP 500.
        # See https://softwareteams.atlassian.net/browse/CICD-5481
        elif self.ref_type == 'tag':
            target['commit'] = {
                'type': 'commit',
                'hash': self._get_tag_target(self.ref_name, self.account, self.repo, auth)
            }
        if self.custom_pipeline:
            target['selector'] = {
                'type': 'custom',
                'pattern': self.custom_pipeline
            }
        if self.pipeline_variables:
            data['variables'] = self.pipeline_variables

        trigger_url = f'{BITBUCKET_API_BASE_URL}/repositories/{self.account}/{self.repo}/pipelines/'
        error_base_message = "Failed to trigger the pipeline."
        response = self._make_session_request("POST", trigger_url, error_base_message, auth=auth, headers=self.headers, json=data)

        if response.status_code == HTTPStatus.NOT_FOUND:
            self.fail("Account, repository or branch doesn't exist.")
        elif response.status_code == HTTPStatus.BAD_REQUEST:
            self.fail(f"Error: {response.text}")
        elif response.status_code == HTTPStatus.UNAUTHORIZED:
            self.fail(
                "API request failed with status 401. Check your account and app password and try again.")
        elif response.status_code != HTTPStatus.CREATED:
            self.fail(
                f"Failed to initiate a pipeline. API error code: {response.status_code}. Message: {response.text}")
        else:
            response_data = response.json()
            build_number = response_data['build_number']
            self.success(f"Build started successfully. Build number: {build_number}."
                         f"\n\tFollow build logs at the following URL: https://bitbucket.org/"
                         f"{self.account}/{self.repo}/addon/pipelines/home#!/results/{build_number}")

        if not self.wait:
            self.success("Pipe finished successfully.", do_exit=True)

        logger.info("Waiting for pipeline to finish")

        deadline = time.time() + self.wait_max_time
        attempt = 0
        retry_on_failure_counter = 0
        build_uuid = response_data['uuid']

        message = ""
        while time.time() < deadline:
            attempt += 1
            logger.info(f"Attempt number {attempt}: Getting build info.")

            success, message = self._check_build(build_number, auth)

            if success is None:
                time.sleep(BITBUCKET_API_POLLING_INTERVAL)
            elif not success:
                if self.max_retries_on_failure is not None and retry_on_failure_counter < self.max_retries_on_failure:
                    retry_on_failure_counter += 1
                    logger.info(f'Retrying failed pipeline (attempt {retry_on_failure_counter} of {self.max_retries_on_failure}).')
                    self._retry_completed_failed_pipeline(build_uuid, auth)
                    time.sleep(BITBUCKET_API_POLLING_INTERVAL)
                else:
                    self.fail(message)
            elif success:
                self.success(message)
                break

        else:
            self.fail(f'Timeout waiting for the pipeline completion. Error: {message}')

        self.success('Pipe finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = TriggerBuild(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
