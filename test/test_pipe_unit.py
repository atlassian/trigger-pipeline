import os

import requests_mock
import pytest
from requests.exceptions import Timeout

from bitbucket_pipes_toolkit.test import PipeTestCase
from pipe.main import TriggerBuild, schema, BITBUCKET_API_BASE_URL, BITBUCKET_INTERNAL_BASE_URL


class StatusBuildTestCase(PipeTestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, mocker, capsys):
        self.mocker = mocker
        self.capsys = capsys

    def test_build_succeeded(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                json={
                    'state': {
                        'name': 'COMPLETED',
                        'type': 'pipeline_state_completed',
                        'result': {
                            'name': 'SUCCESSFUL',
                            'type': 'pipeline_state_completed_successful'
                        }
                    }
                },
                status_code=200
            )

            TriggerBuild(schema=schema, check_for_newer_version=True).run()

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✔ Build number \d+ for [-\w]+@master finished successfully.')
        self.assertRegex(out, '✔ Pipe finished successfully')

    def test_build_succeeded_without_wait(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 0)

        out = self.capsys.readouterr().out
        self.assertRegex(out, '✔ Pipe finished successfully')

    def test_build_failed(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                json={
                    'state': {
                        'name': 'COMPLETED',
                        'type': 'pipeline_state_completed',
                        'result': {
                            'name': 'FAILED',
                            'type': 'pipeline_state_completed_failed'
                        }
                    }
                },
                status_code=200
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Build number \d+ for [-\w]+@master failed.')

    def test_build_error(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                json={
                    'state': {
                        'name': 'COMPLETED',
                        'type': 'pipeline_state_completed',
                        'result': {
                            'name': 'ERROR',
                            'type': 'pipeline_state_completed_error'
                        }
                    }
                },
                status_code=200
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Build number \d+ for [-\w]+@master finished with result ERROR.')

    def test_build_unexpected_state(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                json={
                    'state': {
                        'name': 'OTHER',
                        'type': 'pipeline_other_state'
                    }
                },
                status_code=200
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Unexpected pipeline build state: OTHER')

    def test_get_build_status_retry_timeout(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'WAIT_MAX_TIMEOUT': '10'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                text="Internal Server Error",
                status_code=500
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, (r'✖ Timeout waiting for the pipeline completion. '
                               r'Error: Failed to get the build state for build number 1 '
                               r'for test-trigger-build. Status Code: 500. Error: Internal Server Error.'))

    def test_get_build_status_retry_success(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'WAIT_MAX_TIMEOUT': '10'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            json_state = {
                'state': {
                    'name': 'COMPLETED',
                    'type': 'pipeline_state_completed',
                    'result': {
                        'name': 'SUCCESSFUL',
                        'type': 'pipeline_state_completed_successful'
                    }
                }
            }

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                [
                    {'text': 'Error', 'status_code': 500},
                    {'json': json_state, 'status_code': 200}
                ]
            )

            TriggerBuild(schema=schema, check_for_newer_version=True).run()

        out = self.capsys.readouterr().out
        self.assertEqual(m.call_count, 3)
        self.assertRegex(out,
                         r'✔ Build number \d+ for [-\w]+@master finished successfully.')
        self.assertRegex(out, '✔ Pipe finished successfully')

    def test_get_build_status_timeout_error(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'WAIT_MAX_TIMEOUT': '10'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                exc=Timeout
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Failed to get the build state for build number 1 for test-trigger-build')


class GetTagTimeoutTestCase(PipeTestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, mocker, capsys):
        self.mocker = mocker
        self.capsys = capsys

    def test_get_tag_info_timeout_error(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'REF_TYPE': 'tag',
                'REF_NAME': 'test',
                'WAIT': 'true',
                'WAIT_MAX_TIMEOUT': '10'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/refs/tags/test',
                exc=Timeout
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Failed to get the tag info for test from Bitbucket.')


class TriggerPipelineTimeoutTestCase(PipeTestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, mocker, capsys):
        self.mocker = mocker
        self.capsys = capsys

    def test_get_trigger_pipeline_timeout_error(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'WAIT_MAX_TIMEOUT': '10'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                exc=Timeout
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, r'✖ Failed to trigger the pipeline.')


class TriggerPipelineRetriesTestCase(PipeTestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, mocker, capsys):
        self.mocker = mocker
        self.capsys = capsys

    def test_credential_config_for_max_retries_on_failure(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_ACCESS_TOKEN': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'MAX_RETRIES_ON_FAILURE': '2'
            },
            True
        )

        with self.assertRaises(SystemExit) as exc_context:
            TriggerBuild(schema=schema, check_for_newer_version=True).run()
        self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, 'Validation errors:')
        self.assertRegex(out, 'field \'BITBUCKET_APP_PASSWORD\' is required')
        self.assertRegex(out, 'field \'BITBUCKET_USERNAME\' is required')

    def test_min_config_for_max_retries_on_failure(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_USERNAME': 'test',
                'BITBUCKET_APP_PASSWORD': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'MAX_RETRIES_ON_FAILURE': '0'
            },
            True
        )

        with self.assertRaises(SystemExit) as exc_context:
            TriggerBuild(schema=schema, check_for_newer_version=True).run()
        self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, 'Validation errors:')
        self.assertRegex(out, 'min value is 1')

    def test_max_config_for_max_retries_on_failure(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_USERNAME': 'test',
                'BITBUCKET_APP_PASSWORD': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'MAX_RETRIES_ON_FAILURE': '11'
            },
            True
        )

        with self.assertRaises(SystemExit) as exc_context:
            TriggerBuild(schema=schema, check_for_newer_version=True).run()
        self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertRegex(out, 'Validation errors:')
        self.assertRegex(out, 'max value is 10')

    def test_max_retries_on_failure(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_USERNAME': 'test',
                'BITBUCKET_APP_PASSWORD': 'test',
                'ACCOUNT': 'test',
                'REPOSITORY': 'test-trigger-build',
                'WAIT': 'true',
                'MAX_RETRIES_ON_FAILURE': '2'
            },
            True
        )

        with requests_mock.Mocker() as m:
            m.register_uri(
                'POST',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/',
                json={
                    'uuid': '{180d33b6-fec2-43ab-a21d-c8d93b99aeca}',
                    'build_number': 1
                },
                status_code=201
            )

            m.register_uri(
                'GET',
                f'{BITBUCKET_API_BASE_URL}/repositories/test/test-trigger-build/pipelines/1',
                json={
                    'state': {
                        'name': 'COMPLETED',
                        'type': 'pipeline_state_completed',
                        'result': {
                            'name': 'FAILED',
                            'type': 'pipeline_state_completed_failure'
                        }
                    }
                },
                status_code=200
            )

            m.register_uri(
                'POST',
                f'{BITBUCKET_INTERNAL_BASE_URL}/repositories/test/test-trigger-build/pipelines/%7B180d33b6-fec2-43ab-a21d-c8d93b99aeca%7D/runs',
                status_code=201
            )

            with self.assertRaises(SystemExit) as exc_context:
                TriggerBuild(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        out = self.capsys.readouterr().out
        self.assertEqual(m.call_count, 6)  # 1xCreate, 3xStatus, 2xRetry
        self.assertRegex(out, r'✖ Build number \d+ for [-\w]+@master failed.')
