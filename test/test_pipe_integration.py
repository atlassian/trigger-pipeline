import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class TriggerBuildBaseTestCase(PipeTestCase):

    def run_container(self, add_repo=True, *args, **kwargs):
        kwargs['environment'].update({
            'ACCOUNT': 'bbcitest',
        })

        if add_repo:
            kwargs['environment'].update({
                'REPOSITORY': 'test-trigger-build'
            })

        return super().run_container(*args, **kwargs)


class TriggerBuildTestCase(TriggerBuildBaseTestCase):

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'WAIT': 'true'
        })

        self.assertRegex(
            result, r'✔ Build number \d+ for [-\w]+@master finished successfully')

    def test_build_successful_for_tag(self):
        tag_name = '1.0.0'
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'REF_NAME': tag_name
        })

        self.assertRegex(
            result, rf'✔ Build number \d+ for [-\w]+@{tag_name} finished successfully')

    def test_build_successful_for_commit(self):
        commit_hash = '7861879c940054da25decaa75de9087121953d16'  # hash for tag 1.0.0
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'WAIT': 'true',
            'COMMIT': commit_hash
        })

        self.assertRegex(
            result, r'✔ Build number \d+ for [-\w]+@master finished successfully')

    def test_build_fails_for_wrong_tag(self):
        tag_name = 'no-such-tag'
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'REF_NAME': tag_name
        })

        self.assertRegex(
            result, rf'✖ Failed to get the tag info for {tag_name}')

    def test_build_should_fail_if_downstream_fails(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REF_NAME': ref,
            'WAIT': 'true'
        })

        self.assertRegex(
            result, rf'✖ Build number \d+ for [-\w]+@{ref} failed')

    def test_pipe_should_timeout(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REF_NAME': ref,
            'WAIT': 'true',
            'WAIT_MAX_TIMEOUT': 1
        })

        self.assertRegex(
            result, '✖ Timeout waiting for the pipeline completion')

    def test_build_success_if_manual_step(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REF_NAME': 'develop',
            'WAIT': 'true',
        })

        self.assertRegex(
            result, r'✔ Build number \d+ for [-\w]+@develop is paused')


class InvalidParametersTestCase(TriggerBuildBaseTestCase):

    def test_no_proper_auth(self):
        result = self.run_container(environment={
            'REPOSITORY': 'test-trigger-build'
        })
        self.assertIn('Authentication missing.', result)

    def test_no_repo(self):
        result = self.run_container(add_repo=False, environment={})
        self.assertIn('REPOSITORY:\n- required field', result)

    def test_pipe_fails_for_nonexistent_branch(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REPOSITORY': 'test-trigger-build',
            'REF_NAME': 'no-such-branch',
        })

        self.assertRegex(
            result, r"✖ Account, repository or branch doesn't exist")

    def test_pipe_wait_should_be_boolean(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REPOSITORY': 'test-trigger-build',
            'WAIT': 'not-a-bool',
        })

        self.assertRegex(
            result, r"WAIT:\n- must be of boolean type")

    def test_pipe_should_fail_wrong_account(self):
        result = self.run_container(environment={
            'ACCOUNT': 'no-such-account',
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REPOSITORY': 'test-trigger-build',
            'REF_NAME': 'no-such-branch',
        })

        self.assertRegex(
            result, r"✖ Account, repository or branch doesn't exist")

    def test_pipe_should_fail_wrong_repo(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REPOSITORY': 'no-such-repo-hopefully',
            'REF_NAME': 'no-such-branch',
        })

        self.assertRegex(
            result, r"✖ Account, repository or branch doesn't exist")


class PipelineTypeTestCase(TriggerBuildBaseTestCase):

    def test_custom_with_pattern(self):
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REF_NAME': 'custom-pipeline',
            'CUSTOM_PIPELINE_NAME': 'test-custom',
            'WAIT': 'true'
        })

        self.assertRegex(
            result, r"Build number \d+ for [-\w]+@custom-pipeline finished successfully")


class PipelineVariablesTestCase(TriggerBuildBaseTestCase):

    def test_custom_pipeline_with_variables_should_run(self):
        """
        The downstream pipeline will fail if the MY_VARIABLE is not set.
        """
        variables = """
        [{
            "key": "MY_VARIABLE",
            "value": "$BITBUCKET_BUILD_NUMBER"
          }, {
            "key": "var2key",
            "value": "var2value"
        }]
        """
        result = self.run_container(environment={
            'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
            'REF_NAME': 'master',
            'CUSTOM_PIPELINE_NAME': 'test-custom-variables',
            'WAIT': 'true',
            'PIPELINE_VARIABLES': variables
        })

        self.assertRegex(
            result, r"Build number \d+ for [-\w]+@master finished successfull")
