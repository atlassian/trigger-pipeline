# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 5.9.0

- minor: Add support for REQUEST_READ_TIMEOUT variable. Use REQUEST_READ_TIMEOUT variable to manage response wait times.

## 5.8.2

- patch: Internal maintenance: Add POST requests to session retries.
- patch: Internal maintenance: Update pipes versions in pipelines config file.

## 5.8.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 5.8.0

- minor: Added ability to retry a failed pipeline.

## 5.7.2

- patch: Internal maintenance: Increase requests timeout to 10 seconds.

## 5.7.1

- patch: Internal maintenance: Add test for COMMIT variable support.
- patch: Internal maintenance: Refactor pipe.

## 5.7.0

- minor: Implement session retry support on trigger pipeline and get tag info requests.

## 5.6.0

- minor: Add support for triggering pipelines by commit hash.

## 5.5.1

- patch: Improve error handling to support new pipeline build state i.e. PARSING.

## 5.5.0

- minor: Add support of PARSING build state.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: update requirements.

## 5.4.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update tests to authenticate only via bitbucket access token.

## 5.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 5.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 5.2.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit to fix vulnerability with certify CVE-2023-37920.

## 5.1.3

- patch: Add retry functionality to check the build status.

## 5.1.2

- patch: Add default timeout to the requests to prevent the pipe hanging.

## 5.1.1

- patch: Fix issue with some REPOSITORY name. Now pipe always use lowercase name of the REPOSITORY for API calls.

## 5.1.0

- minor: Add support for token based authentication.
- patch: Internal maintenance: add README example for BITBUCKET_ACCESS_TOKEN usage.
- patch: Internal maintenance: add test to cover authentication.
- patch: Internal maintenance: delete unused ci-scripts.
- patch: Internal maintenance: update docker image to python:3.10-slim.
- patch: Internal maintenance: update release process.
- patch: Internal maintenance: update requirements.

## 5.0.1

- patch: Add details about breaking changes in the pipe started from version 5.0.0.
- patch: Internal maintenance: Update the community link.

## 5.0.0

- major: Add support for REF_NAME and REF_TYPE variables. The pipe supports triggering pipeline by branch and by tag in your Bitbucket repository. Contains breaking changes. Variable BRANCH_NAME is deprecated, use instead REF_NAME and REF_TYPE. More details in Example section of README.md file.

## 4.3.1

- patch: Internal maintenance: Bump version of requests to 2.26.* and bitbucket_pipes_toolkit to 3.2.1

## 4.3.0

- minor: Update README with details of how to trigger advanced example with custom branch.

## 4.2.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 4.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 4.1.7

- patch: Internal maintenance: change pipe metadata according to new structure

## 4.1.6

- patch: Add retry on failed getting build state

## 4.1.5

- patch: Bugfix: Fail if pipeline finished with non-successful status

## 4.1.4

- patch: Internal maintenance: Add gitignore secrets.

## 4.1.3

- patch: Update the Readme with a new Atlassian Community link.

## 4.1.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 4.1.1

- patch: Fixed the handling of the downstream pipelines that halted.

## 4.1.0

- minor: Added a warning message when new version of the pipe is available

## 4.0.5

- patch: Fixed the handling of the downstream pipelines that have a manual step in their configuration.

## 4.0.4

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 4.0.3

- patch: Updated readme BITBUCKET_USERNAME should be an account name, not the email.

## 4.0.2

- patch: Fixed the bug with parsing yaml when special characters are present.

## 4.0.1

- patch: Added code style checks

## 4.0.0

- major: Rollback to using ACCOUNT instead of WORKSPACE

## 3.1.1

- patch: Internal maintenance: update pipes toolkit version.

## 3.1.0

- minor: Fixed a small bug when DEBUG was True by default

## 3.0.2

- patch: Fixed the incorrect error message when workspace, repository or branch doesn't exist

## 3.0.1

- patch: Documentation improvements

## 3.0.0

- major: PIPELINE_TYPE and PIPELINE_PATTERN parameters were replaced by a single CUSTOM_PIPELINE_NAME parameter

## 2.0.1

- patch: Documentation updates

## 2.0.0

- major: Variables names for REPO, ACCOUNT and APP_PASSWORD were changed
- minor: Add support for passing variables to a custom pipeline
- minor: Added support for triggering pipelines in repos that belong to a team

## 1.1.0

- minor: Added support for passing variables to a custom pipeline

## 1.0.0

- major: Parameters names were changes to be more intuitive. Added support to trigger different pipelines definitions

## 0.3.0

- minor: Don't use pipe.yml to specify parameters schema

## 0.2.1

- patch: Patch version bump

## 0.2.0

- minor: Rename the pipe

## 0.1.0

- minor: Initial release
