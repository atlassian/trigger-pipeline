# Bitbucket Pipelines pipe: trigger Bitbucket pipeline

Trigger a pipeline in a Bitbucket repository. You have the ability to specify a branch or a tag and the pipeline to run for the branch. You can also configure this pipe to wait until the triggered pipeline has finished and reported its status. See the **Examples** section for how to configure this pipe.

Started from version 5.0.0 pipe (major update contains breaking changes) supports triggering pipeline by branch and by tag in your Bitbucket repository.
Variable `BRANCH_NAME` is deprecated, use instead `REF_NAME` and `REF_TYPE`. See **Examples** section for more details.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      REPOSITORY: '<string>'
      # BITBUCKET_USERNAME: '<string>' # Optional
      # BITBUCKET_APP_PASSWORD: '<string>' # Optional
      # BITBUCKET_ACCESS_TOKEN: '<string>' # Optional
      # ACCOUNT: '<string>' # Optional
      # REF_TYPE: '<string>' # Optional
      # REF_NAME: '<string>' # Optional
      # COMMIT: '<string>' # Optional
      # CUSTOM_PIPELINE_NAME: '<string>' # Optional
      # PIPELINE_VARIABLES: '<json>' # Optional
      # WAIT: '<boolean>' # Optional
      # WAIT_MAX_TIMEOUT: '<integer>' # Optional
      # REQUEST_READ_TIMEOUT: '<integer>' # Optional
      # MAX_RETRIES_ON_FAILURE: '<integer>' # Optional
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                   | Usage                                                                                                                                                                                                                                                                                                                                      |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| BITBUCKET_USERNAME  (1)    | Bitbucket user that will trigger the pipeline. Note, that this should be an account name, not the email. Required unless `BITBUCKET_ACCESS_TOKEN` is used.                                                                                                                                                                                 |
| BITBUCKET_APP_PASSWORD (1) | [Bitbucket app password][Bitbucket app password] of the user that will trigger the pipeline. Used with `BITBUCKET_USERNAME`. Required unless `BITBUCKET_ACCESS_TOKEN` is used.                                                                                                                                                             |
| BITBUCKET_ACCESS_TOKEN (1) | The [access token][Bitbucket access token] for the repository. Required unless `BITBUCKET_USERNAME` and `BITBUCKET_APP_PASSWORD` are used.                                                                                                                                                                                                 |
| REPOSITORY (*)             | Name of the Bitbucket repository.                                                                                                                                                                                                                                                                                                          |
| ACCOUNT                    | The account or team name of the Bitbucket repository in which you want to trigger a pipeline. Default `$BITBUCKET_REPO_OWNER`.                                                                                                                                                                                                             |
| REF_TYPE                   | The type of the reference to run the pipeline on. Can be `branch` or `tag`. Default `branch`.                                                                                                                                                                                                                                              |
| REF_NAME                   | The name of the reference to run the pipeline on. Default `master`. It should be a branch name if the `REF_TYPE` is `branch`, it should be a tag name if `REF_TYPE` is `tag`.                                                                                                                                                              |
| COMMIT                     | Commit hash to check out. Can be used together with a branch or tag to set the context. Can optionally be run with a custom pipeline - either with or without setting a non-default branch/tag. If unset, the tip of the specified branch will be used.                                                                                    |
| CUSTOM_PIPELINE_NAME       | The name of the custom pipeline to run for a branch. Unset by default, and will run the pipeline defined for the branch. If custom pipeline name is provided, the custom pipeline will be run for for the branch. Prefixing the custom pipeline name with "custom: " is optional.                                                          |
| PIPELINE_VARIABLES         | JSON document containing variables to pass to the pipeline you want to trigger. This currently only works for `custom` piplines. The value should be a list of object with `key`, `value` and `secure` fields for each of your variables. The **Examples** section below contains an example for passing variables to the custom pipeline. |
| WAIT                       | Flag to wait until the triggered build finishes. Default: `false`. If set to `true`, this pipe will wait for the triggered pipeline to complete. If the triggered pipeline fails, this pipe will also fail.                                                                                                                                |
| WAIT_MAX_TIMEOUT           | Maximum time in seconds to wait for the triggered build to complete. Default: `3600`. Pipe will fail if the timeout is reached.                                                                                                                                                                                                            |
| REQUEST_READ_TIMEOUT       | Once the client has connected to the server and sent the HTTP request, the read timeout is the number of seconds the client will wait for the server to send a response. Default: `10`.                                                                                                                                                    |
| MAX_RETRIES_ON_FAILURE     | Maximum retry attempts (integer). Retry attempts are still bound to the global `WAIT_MAX_TIMEOUT`. Requires `WAIT` to be enabled (`true`). Also requires the use of `BITBUCKET_USERNAME` and `BITBUCKET_APP_PASSWORD` authentication methods, as `Retry API` does not support access tokens currently. Min: `1`, Max: `10`.                |
| DEBUG                      | Flag to turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                                                                 |

_(*) = required variable._

_(1) = required variable. Required one of the multiple options._


## Prerequisites

To use this pipe, you need to either [generate an app password][Bitbucket app password] or an [access token][Bitbucket access token]. Remember to check the `Pipelines Write` and `Repositories Read` permissions when generating it. If you want to trigger a pipeline in a repository owned by a team account, make sure you have the correct access to the repository.

## Examples

### Basic examples
This pipe will trigger the branch pipeline for `master` in `your-awesome-repo`. This pipeline will continue, without waiting for the triggered pipeline to complete.

```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
```
This pipe will trigger the branch pipeline for `master` in `your-awesome-repo`. Authentication method is by `BITBUCKET_ACCESS_TOKEN`.

```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_ACCESS_TOKEN: $BITBUCKET_ACCESS_TOKEN
      REPOSITORY: 'your-awesome-repo'
```

This pipe will trigger the branch pipeline for `master` in `your-awesome-repo` that is owned by the `teams-in-space` Bitbucket account.

```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      ACCOUNT: 'teams-in-space'
```

This pipe will trigger the master pipeline in `your-awesome-repo` for the given commit.
```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_ACCESS_TOKEN: $BITBUCKET_ACCESS_TOKEN
      REPOSITORY: 'your-awesome-repo'
      COMMIT: 'aaaaabbbbbccccddddeeeeffff'
```

Same as above, but running a custom pipeline instead of the `master` pipeline.
```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_ACCESS_TOKEN: $BITBUCKET_ACCESS_TOKEN
      REPOSITORY: 'your-awesome-repo'
      COMMIT: 'aaaaabbbbbccccddddeeeeffff'
      CUSTOM_PIPELINE_NAME: 'deployment-pipeline'
```


### Advanced examples
This pipe will trigger the branch pipeline for `develop` in `your-awesome-repo`. This pipeline will wait for the triggered pipeline to complete and will return its status. If the triggered pipeline fails, this pipeline will also fail.

_Note! Make sure that you have such branch in your repository to trigger it. Also, setup pipeline for this branch or have a default pipeline._

```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      REF_TYPE: 'branch'
      REF_NAME: 'develop'
      WAIT: 'true'
```

This pipe will trigger the `deployment-pipeline` for branch `master` in `your-awesome-repo`. This pipe passes 3 variables to the triggered pipeline, which performs a deployment to AWS, using JSON.
```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      REF_TYPE: 'branch'
      REF_NAME: 'master'
      CUSTOM_PIPELINE_NAME: 'deployment-pipeline'
      PIPELINE_VARIABLES: >
          [{
            "key": "AWS_DEFAULT_REGION",
            "value": "us-west-1"
          },
          {
            "key": "AWS_ACCESS_KEY_ID",
            "value": "$AWS_ACCESS_KEY_ID",
            "secured": true
          },
          {
            "key": "AWS_SECRET_ACCESS_KEY",
            "value": "$AWS_SECRET_ACCESS_KEY",
            "secured": true
          }]
      WAIT: 'true'

```

This pipe will trigger the `deployment-pipeline` for commit on tag `1.0.0` in `your-awesome-repo`. This pipe passes 3 variables to the triggered pipeline, which performs a deployment to AWS, using JSON.
```yaml
script:
  - pipe: atlassian/trigger-pipeline:5.9.0
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      REF_TYPE: 'tag'
      REF_NAME: '1.0.0'
      CUSTOM_PIPELINE_NAME: 'deployment-pipeline'
      PIPELINE_VARIABLES: >
          [{
            "key": "AWS_DEFAULT_REGION",
            "value": "us-west-1"
          },
          {
            "key": "AWS_ACCESS_KEY_ID",
            "value": "$AWS_ACCESS_KEY_ID",
            "secured": true
          },
          {
            "key": "AWS_SECRET_ACCESS_KEY",
            "value": "$AWS_SECRET_ACCESS_KEY",
            "secured": true
          }]
      WAIT: 'true'

```

## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on the Atlassian Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[Bitbucket app password]: https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html
[Bitbucket access token]: https://support.atlassian.com/bitbucket-cloud/docs/access-tokens/
[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,trigger
